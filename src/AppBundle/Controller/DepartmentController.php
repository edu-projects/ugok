<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 24.05.2015
 * Time: 1:17
 */

namespace AppBundle\Controller;




use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DepartmentController extends Controller {

    public function indexAction()
    {
        $departments = $this->getDoctrine()
            ->getRepository('AppBundle:Department')
            ->findAll();
        return $this->render('AppBundle:department:index.html.twig', array('departments'=>$departments));
    }
    public function detailAction($id)
    {
        $department = $this->getDoctrine()
            ->getRepository('AppBundle:Department')
            ->find($id);
        if (!$department) throw new NotFoundHttpException;
        $emploee = $this->getDoctrine()
            ->getRepository('AppBundle:Account')
            ->findBy(array('department'=> $id));
        return $this->render('AppBundle:department:detail.html.twig', array('e'=>$emploee));
    }
}