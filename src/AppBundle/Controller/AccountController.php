<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 23.05.2015
 * Time: 21:31
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\Registration;
use AppBundle\Form\RegistrationType;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\SecurityContext;


class AccountController extends Controller {

    public function registrationAction()
    {
        $registration = new Registration();
        $form = $this->createForm(new RegistrationType(), $registration, array(
            'action' => $this->generateUrl('account_create'),
        ));

        return $this->render(
            'AppBundle:account:registration.html.twig',
            array('form' => $form->createView())
        );
    }
    public function createAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new RegistrationType(), new Registration());

        $form->handleRequest($request);

        if ($form->isValid()) {
            $registration = $form->getData();

            $factory = $this->get('security.encoder_factory');
            $account =  $registration->getAccount();

            $encoder = $factory->getEncoder($account);

            $password = $encoder->encodePassword($account->getPassword(), $account->getSalt());
            $account->setPassword($password);
            $account->setActive(false);


            $em->persist($account);

            $em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('Подтверждение регистрации')
                ->setFrom('mailer@dvstorage.ru')
                ->setTo($account->getEmail())

                ->setBody($this->renderView('AppBundle:account:validate.txt.twig', array('link'=>$this->generateUrl('account_validate', array('code'=>$account->getActivationCode()),true)))
                );
            $this->get('mailer')->send($message);

            return $this->render('AppBundle:account:validate.html.twig', array('email'=>$account->getEmail()));
        }

        return $this->render(
            'AppBundle:account:registration.html.twig',
            array('form' => $form->createView())
        );
    }
    public function validateAction($code)
    {
        $em = $this->getDoctrine()->getManager();
        try{
            $entity = $em->getRepository('AppBundle:Account')->loadUserByCode($code);
            $entity->setActive(true);
            $entity->setActivationCode(null);

            $em->persist($entity);
            $em->flush();

            return $this->render(
                'AppBundle:account:validate_success.html.twig'
            );
        }
        catch (UsernameNotFoundException $e)
        {
            return $this->render(
                'AppBundle:account:validate_fail.html.twig', array('error'=> $e->getMessage())
            );
        }

    }
    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            return $this->redirect($this->generateUrl('homepage'));
        }

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR))
        {
            $error = $request->attributes-get(SecurityContext::AUTHENTICATION_ERROR);
        }
        else
        {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('AppBundle:account:login.html.twig',
            array('last_username'=>$session->get(SecurityContext::LAST_USERNAME),
                'error'=> $error,
            ));
    }
    public function accountLockAction($id)
    {
        $account = $this->getDoctrine()
            ->getRepository('AppBundle:Account')
            ->find($id);
        if (!$account) {
            throw $this->createNotFoundException(
                'No account found for id '.$id
            );
        }
        $account->setBlocked(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();

        $response = new Response();
        $response->setContent(json_encode(array('result'=>'ok')));
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }
    public function accountUnlockAction($id)
    {
        $account = $this->getDoctrine()
            ->getRepository('AppBundle:Account')
            ->find($id);
        if (!$account) {
            throw $this->createNotFoundException(
                'No account found for id '.$id
            );
        }
        $account->setBlocked(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();

        $response = new Response();
        $response->setContent(json_encode(array('result'=>'ok')));
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }
}