<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 24.05.2015
 * Time: 1:56
 */

namespace AppBundle\Controller;


use AppBundle\Entity\News;
use AppBundle\Entity\Vacancy;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\NewsAddType;
use AppBundle\Form\VacancyAddType;
use Symfony\Component\HttpFoundation\Request;


class AdministrationController extends Controller{

    public function indexAction()
    {
        $tickets = $this->getDoctrine()
            ->getRepository('AppBundle:ServiceOrder')
            ->findBy(array(), array('creation_date'=>'DESC'));

        return $this->render('AppBundle:administration:index.html.twig', array('tickets'=>$tickets));
    }
    public function usersIndexAction()
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:Account')
            ->findAll();
        return $this->render('AppBundle:administration/users:index.html.twig',
            array('users'=>$users));
    }
    public function newsIndexAction()
    {
        $news = $this->getDoctrine()
            ->getRepository('AppBundle:News')
            ->findAll();
        return $this->render('AppBundle:administration/news:index.html.twig',
            array('news'=>$news));
    }
    public function vacanciesIndexAction()
    {
        $vacancies = $this->getDoctrine()
            ->getRepository('AppBundle:Vacancy')
            ->findAll();
        return $this->render('AppBundle:administration/vacancies:index.html.twig',
            array('vacancies'=>$vacancies));
    }
    public function vacanciesAddAction()
    {
        $vacancy = new Vacancy();

        $form = $this->createForm(new VacancyAddType(), $vacancy, array(
            'action' => $this->generateUrl('administration_vacancies_add_form_check'),
        ));

        return $this->render(
            'AppBundle:administration:vacancies/add.html.twig',
            array('form' => $form->createView())
        );
    }
    public function vacanciesCreateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new VacancyAddType(), new Vacancy());

        $form->handleRequest($request);
        if ($form->isValid()) {
            $vacancy = $form->getData();
            $vacancy->setAccount($this->getUser());
            $em->persist($vacancy);
            $em->flush();

            $this->redirect($this->generateUrl('administration_vacancies_index'));
        }
        return $this->render(
            'AppBundle:administration:vacancies/add.html.twig',
            array('form' => $form->createView())
        );
    }
    public function newsAddAction()
    {
        $news = new News();

        $form = $this->createForm(new NewsAddType(), $news, array(
            'action' => $this->generateUrl('administration_news_add_form_check'),
        ));

        return $this->render(
            'AppBundle:administration:news/add.html.twig',
            array('form' => $form->createView())
        );
    }
    public function newsCreateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new NewsAddType(), new News());

        $form->handleRequest($request);
        if ($form->isValid()) {
            $news_item = $form->getData();
            $news_item->setAccount($this->getUser());
            $em->persist($news_item);
            $em->flush();

            $this->redirect($this->generateUrl('administration_news_index'));
        }
        return $this->render(
            'AppBundle:administration:news/add.html.twig',
            array('form' => $form->createView())
        );
    }
}