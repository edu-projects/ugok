<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 04.06.2015
 * Time: 22:28
 */

namespace AppBundle\Controller;


use AppBundle\Entity\OrderDocument;
use AppBundle\Entity\ServiceOrder;
use AppBundle\Entity\TypeOrder;
use AppBundle\Form\DocumentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ServiceController extends Controller{

    public function indexAction()
    {
        return $this->render('AppBundle:service:index.html.twig');
    }
    public function directionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();
        $entities = $em
            ->getRepository('AppBundle:ServiceOrder')
            ->createQueryBuilder('service')
            ->join('service.status', 'status')
            ->join('service.orderType', 'type')
            ->join('service.account', 'account')
            ->where('status.id <> 3')
            ->andWhere('status.id <> 4')
            ->andWhere('type.id = 1')
            ->andWhere("account.id = {$userId}")
            ->getQuery()
            ->getResult();

        if ($entities)
        {
            return $this->render('AppBundle:service:direction.html.twig',
                array('error'=> 'Вы уже записывались на прием, ваша заявка все еще находится в обработке'));
        }
        $service = new ServiceOrder();
        $service->setAccount($this->getUser());

        $orderType = $this->getDoctrine()
            ->getRepository('AppBundle:TypeOrder')
            ->find(1);
        $status = $this->getDoctrine()
            ->getRepository('AppBundle:Status')
            ->find(1);


        $service->setOrderType($orderType); // director
        $service->setStatus($status); // new doc

        $em->persist($service);
        $em->flush();

        dump($service);

        return $this->render('AppBundle:service:direction.html.twig', array('success'=>true, 'CreatedId'=>$service->getId()));
    }
    public function documentsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();

        $service = new ServiceOrder();
        $service->setAccount($this->getUser());

        $orderType = $this->getDoctrine()
            ->getRepository('AppBundle:TypeOrder')
            ->find(2);
        $status = $this->getDoctrine()
            ->getRepository('AppBundle:Status')
            ->find(1);
        $service->setOrderType($orderType); // doc
        $service->setStatus($status); // new doc


        $orderDocument = new OrderDocument();
        $orderDocument->setServiceOrder($service);

        $form = $this->createForm(new DocumentType(), $orderDocument);


        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            $data = $form->getData();
            $em->persist($data);
            $em->flush();
            return $this->render('AppBundle:service:documents.html.twig',
                array('form'=>$form->createView(),
                    'CreatedId'=>$service->getId()));
        }

        return $this->render('AppBundle:service:documents.html.twig', array('form'=>$form->createView()));
    }
}