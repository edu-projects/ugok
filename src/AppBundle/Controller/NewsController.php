<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 20.05.2015
 * Time: 1:29
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NewsController extends Controller {

    public function indexAction()
    {
        $news = $this->getDoctrine()
            ->getRepository('AppBundle:News')
            ->findBy(array(),array('id'=>'DESC'),6);

        return $this->render('AppBundle:news:index.html.twig', array('news'=>$news));
    }
    public function showNewsDetailAction($id)
    {
        $news = $this->getDoctrine()
            ->getRepository('AppBundle:News')
            ->find($id);

        return $this->render('AppBundle:news:detail.html.twig', array('news'=>$news));
    }
}