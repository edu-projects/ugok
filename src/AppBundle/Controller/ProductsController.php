<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 04.06.2015
 * Time: 20:50
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProductsController extends Controller{

    public function indexAction()
    {
        return $this->render('AppBundle:static:products.html.twig');
    }
}