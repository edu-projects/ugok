<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 20.05.2015
 * Time: 2:27
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AboutController extends Controller{
    public function indexAction()
    {
        return $this->render('AppBundle:static:about.html.twig');
    }
    public function contactsAction(Request $request)
    {
        $data = array();
        $form = $this->createFormBuilder($data)
            ->add('name', 'text')
            ->add('email', 'email')
            ->add('phone', 'text')
            ->add('question', 'textarea')
            ->add('sbn', 'submit')
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            $data = $form->getData();

            $message = \Swift_Message::newInstance()
                ->setSubject('UGOK: Обратная связь')
                ->setFrom('mailer@dvstorage.ru')
                ->setTo(array('togis65@gmail.com', 'adm_ugok@mail.ru'))

                ->setBody($this->renderView('AppBundle:mail:feedback.txt.twig', array('data'=> $data))
                );
            $this->get('mailer')->send($message);

            return $this->render('AppBundle:static:contacts.html.twig', array('form'=>$form->createView(), 'success'=>true));
        }

        return $this->render('AppBundle:static:contacts.html.twig', array('form'=>$form->createView()));
    }
    public function vacancyListAction()
    {
        $vacancies = $this->getDoctrine()
            ->getRepository('AppBundle:Vacancy')
            ->findBy(array(),array('id'=>'DESC'),10);
        return $this->render('AppBundle:vacancy:index.html.twig', array('vacancies'=>$vacancies));
    }
    public function showVacancyDetailAction($id)
    {
        $vacancy = $this->getDoctrine()
            ->getRepository('AppBundle:Vacancy')
            ->find($id);
        if (!$vacancy) throw new NotFoundHttpException;

        return $this->render('AppBundle:vacancy:detail.html.twig', array('vacancy'=>$vacancy));
    }
}