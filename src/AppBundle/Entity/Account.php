<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="account")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AccountRepository")
 */
class Account implements AdvancedUserInterface, \Serializable{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    protected $email;
    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $login;
    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\NotBlank()
     * @Assert\Length(max = 4096)
     */
    protected $password;
    /**
     * @ORM\Column(type="date")
     */
    protected $date_birth;
    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $last_name;
    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $first_name;
    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $middle_name;
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $phone;
    /**
     * @ORM\Column(type="string", nullable=true, length=200)
     */
    protected $active;
    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $salt;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $blocked;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $registration_date;
    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $position;
    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $role;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $activation_code;

    /**
     * @ORM\OneToMany(targetEntity="News", mappedBy="account")
     */
    protected $news;

    /**
     * @ORM\OneToMany(targetEntity="Vacancy", mappedBy="account")
     */
    protected $vacancies;

    /**
     * @ORM\ManyToOne(targetEntity="Department", inversedBy="accounts")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     */
    protected $department;

    /**
     * @ORM\OneToMany(targetEntity="ServiceOrder", mappedBy="account")
     */
    protected $orders;


    function __construct()
    {
        $this->news = new ArrayCollection();
        $this->vacancies = new ArrayCollection();
        $this->orders = new ArrayCollection();
        // setup vars;
        $this->active = false;
        $this->salt = md5(uniqid(null, true));
        $this->activation_code = md5(uniqid(null, true));
        $this->blocked = false;
        $this->registration_date = new \DateTime();
        $this->role = 'ROLE_USER';
    }


    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return !($this->blocked);
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return $this->active;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        return array($this->role);
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
       return null;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->salt,
            $this->active,
            $this->blocked
        ));
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
            $this->active,
            $this->blocked
            ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Account
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set login
     *
     * @param string $login
     * @return Account
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string 
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Account
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set date_birth
     *
     * @param \DateTime $dateBirth
     * @return Account
     */
    public function setDateBirth($dateBirth)
    {
        $this->date_birth = $dateBirth;

        return $this;
    }

    /**
     * Get date_birth
     *
     * @return \DateTime 
     */
    public function getDateBirth()
    {
        return $this->date_birth;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return Account
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return Account
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set middle_name
     *
     * @param string $middleName
     * @return Account
     */
    public function setMiddleName($middleName)
    {
        $this->middle_name = $middleName;

        return $this;
    }

    /**
     * Get middle_name
     *
     * @return string 
     */
    public function getMiddleName()
    {
        return $this->middle_name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Account
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set active
     *
     * @param string $active
     * @return Account
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return string 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return Account
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set blocked
     *
     * @param boolean $blocked
     * @return Account
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;

        return $this;
    }

    /**
     * Get blocked
     *
     * @return boolean 
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * Set registration_date
     *
     * @param \DateTime $registrationDate
     * @return Account
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registration_date = $registrationDate;

        return $this;
    }

    /**
     * Get registration_date
     *
     * @return \DateTime 
     */
    public function getRegistrationDate()
    {
        return $this->registration_date;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Account
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Account
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add news
     *
     * @param \AppBundle\Entity\News $news
     * @return Account
     */
    public function addNews(\AppBundle\Entity\News $news)
    {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param \AppBundle\Entity\News $news
     */
    public function removeNews(\AppBundle\Entity\News $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Add vacancies
     *
     * @param \AppBundle\Entity\Vacancy $vacancies
     * @return Account
     */
    public function addVacancy(\AppBundle\Entity\Vacancy $vacancies)
    {
        $this->vacancies[] = $vacancies;

        return $this;
    }

    /**
     * Remove vacancies
     *
     * @param \AppBundle\Entity\Vacancy $vacancies
     */
    public function removeVacancy(\AppBundle\Entity\Vacancy $vacancies)
    {
        $this->vacancies->removeElement($vacancies);
    }

    /**
     * Get vacancies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVacancies()
    {
        return $this->vacancies;
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     * @return Account
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\Department 
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Add orders
     *
     * @param \AppBundle\Entity\ServiceOrder $orders
     * @return Account
     */
    public function addOrder(\AppBundle\Entity\ServiceOrder $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \AppBundle\Entity\ServiceOrder $orders
     */
    public function removeOrder(\AppBundle\Entity\ServiceOrder $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set activation_code
     *
     * @param string $activationCode
     * @return Account
     */
    public function setActivationCode($activationCode)
    {
        $this->activation_code = $activationCode;

        return $this;
    }

    /**
     * Get activation_code
     *
     * @return string 
     */
    public function getActivationCode()
    {
        return $this->activation_code;
    }
}
