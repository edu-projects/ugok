<?php
namespace AppBundle\Entity;


use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class AccountRepository extends EntityRepository implements UserProviderInterface {

    public function loadUserByUsername($email)
    {

        $q = $this
            ->createQueryBuilder('a')
            ->where('a.email = :email')
            ->setParameter('email', $email)
            ->getQuery();

        try {
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find an active admin User object identified by "%s".',
                $email
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }
        return $user;
    }
    public function loadUserByCode($code)
    {

        $q = $this
            ->createQueryBuilder('a')
            ->where('a.activation_code = :code')
            ->setParameter('code', $code)
            ->getQuery();

        try {
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find security code'
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }
        return $user;
    }
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
        || is_subclass_of($class, $this->getEntityName());
    }
}