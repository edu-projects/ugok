<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="orderDocument")
 */
class OrderDocument {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="integer")
     */
    protected $amount;

    /**
     * @ORM\ManyToOne(targetEntity="Document", inversedBy="order_documents")
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id")
     */
    protected $document;

    /**
     * @ORM\ManyToOne(targetEntity="ServiceOrder", inversedBy="order_document", cascade={"persist"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    protected $service_order;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return OrderDocument
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set id_order
     *
     * @param integer $idOrder
     * @return OrderDocument
     */
    public function setIdOrder($idOrder)
    {
        $this->id_order = $idOrder;

        return $this;
    }

    /**
     * Get id_order
     *
     * @return integer
     */
    public function getIdOrder()
    {
        return $this->id_order;
    }

    /**
     * Set document
     *
     * @param \AppBundle\Entity\Document $document
     * @return OrderDocument
     */
    public function setDocument(\AppBundle\Entity\Document $document = null)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \AppBundle\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set service_order
     *
     * @param \AppBundle\Entity\ServiceOrder $serviceOrder
     * @return OrderDocument
     */
    public function setServiceOrder(\AppBundle\Entity\ServiceOrder $serviceOrder = null)
    {
        $this->service_order = $serviceOrder;

        return $this;
    }

    /**
     * Get service_order
     *
     * @return \AppBundle\Entity\ServiceOrder 
     */
    public function getServiceOrder()
    {
        return $this->service_order;
    }
}
