<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="vacancy")
 */
class Vacancy {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", length=400)
     */
    protected $name;
    /**
     * @ORM\Column(type="string", length=400)
     */
    protected $requirement; //требования
    /**
     * @ORM\Column(type="string", length=400)
     */
    protected $preferably; //желательно
    /**
     * @ORM\Column(type="decimal")
     */
    protected $salary;
    /**
     * @ORM\Column(type="string", length=400)
     */
    protected $contacts;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $publication_date;

    /**
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="vacancies")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    protected $account;


    function __construct()
    {
        $this->publication_date = new \DateTime();

    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Vacancy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set requirement
     *
     * @param string $requirement
     * @return Vacancy
     */
    public function setRequirement($requirement)
    {
        $this->requirement = $requirement;

        return $this;
    }

    /**
     * Get requirement
     *
     * @return string 
     */
    public function getRequirement()
    {
        return $this->requirement;
    }

    /**
     * Set preferably
     *
     * @param string $preferably
     * @return Vacancy
     */
    public function setPreferably($preferably)
    {
        $this->preferably = $preferably;

        return $this;
    }

    /**
     * Get preferably
     *
     * @return string 
     */
    public function getPreferably()
    {
        return $this->preferably;
    }

    /**
     * Set salary
     *
     * @param string $salary
     * @return Vacancy
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary
     *
     * @return string 
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Set contacts
     *
     * @param string $contacts
     * @return Vacancy
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * Get contacts
     *
     * @return string 
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Set publication_date
     *
     * @param \DateTime $publicationDate
     * @return Vacancy
     */
    public function setPublicationDate($publicationDate)
    {
        $this->publication_date = $publicationDate;

        return $this;
    }

    /**
     * Get publication_date
     *
     * @return \DateTime 
     */
    public function getPublicationDate()
    {
        return $this->publication_date;
    }


    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     * @return Vacancy
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
