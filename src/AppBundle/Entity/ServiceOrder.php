<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="serviceOrder")
 */

class ServiceOrder {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="datetime")
     *
     */
    protected $creation_date;

    /**
     * @ORM\ManyToOne(targetEntity="TypeOrder", inversedBy="orders")
     * @ORM\JoinColumn(name="order_type_id", referencedColumnName="id")
     */
    protected $orderType;

    /**
     * @ORM\ManyToOne(targetEntity="Status", inversedBy="orders")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="orders")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    protected $account;

    /**
     * @ORM\OneToMany(targetEntity="OrderDocument", mappedBy="service_order")
     */
    protected $order_document;

    function __construct()
    {
        $this->order_document = new ArrayCollection();
        $this->creation_date = new \DateTime('now');
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creation_date
     *
     * @param \DateTime $creationDate
     * @return ServiceOrder
     */
    public function setCreationDate($creationDate)
    {
        $this->creation_date = $creationDate;

        return $this;
    }

    /**
     * Get creation_date
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creation_date;
    }


    /**
     * Set orderType
     *
     * @param \AppBundle\Entity\TypeOrder $orderType
     * @return ServiceOrder
     */
    public function setOrderType(\AppBundle\Entity\TypeOrder $orderType = null)
    {
        $this->orderType = $orderType;

        return $this;
    }

    /**
     * Get orderType
     *
     * @return \AppBundle\Entity\TypeOrder 
     */
    public function getOrderType()
    {
        return $this->orderType;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\Status $status
     * @return ServiceOrder
     */
    public function setStatus(\AppBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\Status 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     * @return ServiceOrder
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add order_document
     *
     * @param \AppBundle\Entity\OrderDocument $orderDocument
     * @return ServiceOrder
     */
    public function addOrderDocument(\AppBundle\Entity\OrderDocument $orderDocument)
    {
        $this->order_document[] = $orderDocument;

        return $this;
    }

    /**
    /**s
     * Remove order_document
     *
     * @param \AppBundle\Entity\OrderDocument $orderDocument
     */
    public function removeOrderDocument(\AppBundle\Entity\OrderDocument $orderDocument)
    {
        $this->order_document->removeElement($orderDocument);
    }

    /**
     * Get order_document
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderDocument()
    {
        return $this->order_document;
    }
}
