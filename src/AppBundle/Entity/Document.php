<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="document")
 */
class Document {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="OrderDocument", mappedBy="document")
     */
    protected $order_documents;

    function __construct()
    {
        $this->order_documents = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add order_documents
     *
     * @param \AppBundle\Entity\OrderDocument $orderDocuments
     * @return Document
     */
    public function addOrderDocument(\AppBundle\Entity\OrderDocument $orderDocuments)
    {
        $this->order_documents[] = $orderDocuments;

        return $this;
    }

    /**
     * Remove order_documents
     *
     * @param \AppBundle\Entity\OrderDocument $orderDocuments
     */
    public function removeOrderDocument(\AppBundle\Entity\OrderDocument $orderDocuments)
    {
        $this->order_documents->removeElement($orderDocuments);
    }

    /**
     * Get order_documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderDocuments()
    {
        return $this->order_documents;
    }
}
