<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 24.05.2015
 * Time: 14:19
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VacancyAddType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', 'text');
        $builder->add('requirement', 'textarea');
        $builder->add('preferably', 'textarea');
        $builder->add('salary', 'textarea');
        $builder->add('contacts', 'textarea');
        $builder->add('add', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Vacancy',
        ));
    }

    public function getName()
    {
        return 'VacancyAdd';
    }
}