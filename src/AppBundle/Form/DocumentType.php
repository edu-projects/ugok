<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('amount', 'number');
        $builder->add('document', 'entity', array(
            'class' => 'AppBundle:Document',
            'property' => 'name',
        ));
        $builder->add('add', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\OrderDocument',
        ));
    }

    public function getName()
    {
        return 'DocumentAdd';
    }
}