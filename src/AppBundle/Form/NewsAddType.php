<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 24.05.2015
 * Time: 14:19
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsAddType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('title', 'text');
        $builder->add('text', 'textarea');
        $builder->add('add', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\News',
        ));
    }

    public function getName()
    {
        return 'NewsAdd';
    }
}