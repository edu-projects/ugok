<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 13.01.2015
 * Time: 1:25
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
//use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\True as Recaptcha;
use Symfony\Component\Validator\Constraints\Collection;

class AccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        for ($i = 2015; $i>=1900; $i--)
            $years[] = $i;
        $builder->add('email', 'email');
        $builder->add('password', 'repeated', array(
            'first_name'  => 'password',
            'second_name' => 'confirm',
            'type'        => 'password',
        ));
        $builder->add('date_birth', 'date', array(
            'input'  => 'datetime',
            'widget' => 'choice',
            'years'  => $years,
            'html5' => true,
        ));
        $builder->add('position', 'text');
        $builder->add('department', 'entity', array(
            'class' => 'AppBundle:Department',
            'property' => 'name',
        ));
        $builder->add('last_name', 'text');
        $builder->add('first_name', 'text');
        $builder->add('middle_name', 'text');

        $builder->add('phone', 'text');

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
//        $collectionConstraint = new Collection(array(
//            'recaptcha' => new Recaptcha(),
//        ));

        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Account',
            //'validation_constraint' => $collectionConstraint
        ));
    }

    public function getName()
    {
        return 'account';
    }
}