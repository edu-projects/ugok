<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 13.01.2015
 * Time: 1:30
 */

namespace AppBundle\Form;
use Symfony\Component\Validator\Constraints as Assert;

use AppBundle\Entity\Account;

class Registration {
    /**
     * @Assert\Type(type="AppBundle\Entity\Account")
     * @Assert\Valid()
     */
    protected $account;



    public function setAccount(Account $account)
    {
        $this->account = $account;
    }

    public function getAccount()
    {
        return $this->account;
    }


}