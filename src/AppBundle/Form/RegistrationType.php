<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 13.01.2015
 * Time: 1:33
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends  AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('account', new AccountType());

        $builder->add('Register', 'submit');
    }

    public function getName()
    {
        return 'registration';
    }
}