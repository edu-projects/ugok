<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 23.05.2015
 * Time: 1:30
 */

namespace AppBundle\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NewsControllerTest extends WebTestCase{
    //
    public function testIndex()
    {
        $client = static::createClient(array('environment' => 'test'));

        $crawler = $client->request('GET', '/news/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("Последние новости")')->count() > 0);
    }
}